import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Program {
    public static void main(String[] args) {
        actionWithSerial();
        operationsWithFiles();
    }

    public static void operationsWithFiles(){
        File source = new File("Files/source.txt");
        File dist = new File("Files/dist.txt");

        Date dateStart = new Date();
        System.out.println("\nНачало побайтового копирования");
        slowCopy(source,dist);
        Date dateEnd = new Date();
        System.out.println("Конец побайтового копирования");
        long time = dateEnd.getTime() - dateStart.getTime();
        System.out.println("Время исполнения: " + time + "мс");

        System.out.println("Начало буферизированного копирования");
        Date dateBuffStart = new Date();
        fastCopy(source,dist);
        Date dateBuffEnd = new Date();
        System.out.println("Конец буферизированного копирования");
        long buffTime = dateBuffEnd.getTime() - dateBuffStart.getTime();
        System.out.println("Время исполнения: " + buffTime + "мс");

        System.out.println("Буферизированное копирование работает в : "+ (time/buffTime)+ " раз быстрее!");
    }

    public static void slowCopy(File source,File dist){
        try(InputStream inputStream = new FileInputStream(source);
            OutputStream outputStream = new FileOutputStream(dist)){
            int i = -1;
            while ((i=inputStream.read())!=-1){
                outputStream.write(i);
            }
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void fastCopy(File source,File dist){
        try (var in = new BufferedInputStream(
                new FileInputStream(source));
             var out = new BufferedOutputStream(
                     new FileOutputStream(dist))) {
            var buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer))>0){
                out.write(buffer,0,lengthRead);
                out.flush();
            }

        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public static void lessFastCopy(File source,File dist){
        try (var reader = new BufferedReader(new FileReader(source));
             var writer = new BufferedWriter(new FileWriter(dist))) {
            String s;
            while ((s = reader.readLine()) != null) {
                writer.write(s);
                writer.newLine();
            }
        }
    catch (IOException e){
            e.printStackTrace();
    }
    }

    public static void actionWithSerial(){
        File serFile = new File("Files/forSerial.txt");
        List<Person> people = new ArrayList<>();
        people.add(new Person("Roman",22,"123456"));
        people.add(new Person("Oleg",37,"654321"));
        people.add(new Person("Igor",68,"101010"));
        for (var person : people)
            System.out.println(person);

        saveToFile(people,serFile);
        readFromFile(serFile);
        var peopleAfterSerial = readFromFile(serFile);
        System.out.println("\nОбъекты после десериализации:\n");
        for (var person: peopleAfterSerial)
            System.out.println(person);
    }

    public static void saveToFile(List<Person> people, File dataFile) {
        try (var out = new ObjectOutputStream(
                new BufferedOutputStream(
                        new FileOutputStream(dataFile)))) {
            for (var person : people)
                out.writeObject(person);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public static List<Person> readFromFile(File file){
        var people = new ArrayList<Person>();
        try (var in = new ObjectInputStream(
                new BufferedInputStream(
                        new FileInputStream(file)))) {
            while (true) {
                var object = in.readObject();
                if (object instanceof Person)
                    people.add((Person) object);
            }
        } catch (IOException | ClassNotFoundException ignored) {

        }
        return people;
    }

}
